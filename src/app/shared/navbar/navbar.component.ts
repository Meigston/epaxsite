import { Component, ElementRef, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { PageBase } from 'app/infra/page-base';
import { Router, NavigationEnd } from '@angular/router';
import { BreadcrumbService } from 'xng-breadcrumb';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent extends PageBase {
    static breadscumb: Array<{ label: string, routeLink: string, active: boolean }>;
    private toggleButton: any;
    private sidebarVisible: boolean;

    constructor(public location: Location,
        private element: ElementRef,
        googleAnalyticsService: GoogleAnalyticsService,
        private router: Router,
        private breadcrumbService: BreadcrumbService) {
        super(googleAnalyticsService);

        this.sidebarVisible = false;
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.sidebarClose();
                const route = event as any;

                if (route.url) {
                    NavbarComponent.breadscumb = [];

                    const path = this.location.prepareExternalUrl(this.location.path()).replace(/(#.+[A-Za-z])/, '');
                    const splitRoutes = (route.url as string).split('/');
                    const routerConcat = [];

                    if (splitRoutes.length > 0 && splitRoutes[0] === '' || splitRoutes[0] === 'home') {
                        splitRoutes[0] = 'home';
                    }

                    splitRoutes.forEach(item => {
                        if (item) {
                            item = item.replace(/(#.+[A-Za-z])/, '');
                            routerConcat.push(item);
                            const routeLink = routerConcat.join('/');
                            const objectRouter = this.router.config.filter(f => f.path == routeLink || f.path == item);
                            const active = ('/' + routeLink.replace('home/', '') !== path) && (routeLink.search('post') === -1)

                            let label = '';
                            if (objectRouter && objectRouter.length > 0) {
                                label = objectRouter[0].data.breadcrumb;
                                ////console.log('label', label);
                            }

                            if (!routeLink.match(/(post\/.+|[0-9])/)) {
                                NavbarComponent.breadscumb.push({
                                    label: label ? label : item,
                                    active: active,
                                    routeLink: '/' + routeLink
                                });
                            }
                            NavbarComponent.breadscumb.forEach(item => {
                                item.routeLink = item.routeLink.replace('/home', '');
                            })
                        }
                    });
                }
            }
        });
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    }

    breadsCumList() {
        return NavbarComponent.breadscumb;
    }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];

        setTimeout(function() {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    }

    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    }

    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    }

    isHome() {
        let titlee = this.location.prepareExternalUrl(this.location.path());

        titlee = titlee.charAt(0) === '/' ? titlee.slice(1) : '';

        if (titlee === 'home' || titlee === '') {
            return true;
        }


        const arrayUrl = titlee.split('/');
        if (arrayUrl[arrayUrl.length - 1] === 'formulario') {
            return true;
        }

        return false;
    }
}
