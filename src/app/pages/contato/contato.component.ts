import { Component } from '@angular/core';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { PageBase } from 'app/infra/page-base';
import { ContatoModel } from 'app/models/ContatoModel';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-contato',
    templateUrl: './contato.component.html',
    styleUrls: ['./contato.component.scss']
})

export class ContatoComponent extends PageBase {

    lat: number = -25.388720;
    lng: number = -51.460110;
    zoom = 15;

    constructor(googleAnalyticsService: GoogleAnalyticsService,
        _router: Router,
        titleService: Title) {
        super(googleAnalyticsService, _router, titleService);
        this.addTitlePage('Contato| Epax Construtora');
    }

    ngOnInit() { }
}
