import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContatoComponent } from './contato/contato.component';
import { SobreComponent } from './sobre/sobre.component';
import { EmpreendimentosComponent } from './empreendimentos/empreendimentos.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { BlogComponent } from './blog/blog.component';
import { Page404Component } from './page-not-found/page404.component';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { AgmCoreModule } from '@agm/core';
import { SplendorComponent } from './empreendimentos/splendor/splendor.component';
import { EleganceComponent } from './empreendimentos/elegance/elegance.component';
import { PostComponent } from './blog/post/post.component';
import { FormularioEnvioComponent } from './formulario-envio/formulario-envio.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { BreadcrumbModule } from 'xng-breadcrumb';
import { NgxLoadingModule } from 'ngx-loading';
import { ContatoFormularioComponent } from 'app/components/formulario/contato/contato-formulario.component';
import { LocalizacaoMapsComponent } from 'app/components/maps/localizacao/localizacao.component';
import { CardConteudoComponent } from 'app/components/card-conteudo/card-conteudo.component';
import { TrabalheConoscoComponent } from './trabalhe-conosco/trabalhe-conosco.component';
import { CampoErroComponent } from 'app/components/formulario/campo-erro/campo-erro.component';
import { BannerSlideComponent } from '../components/banner-slide/banner-slide.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { ImageSlideComponent } from 'app/components/image-slide/image-slide.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NouisliderModule,
        RouterModule,
        JwBootstrapSwitchNg2Module,
        GalleryModule,
        AgmCoreModule,
        NgImageSliderModule,
        NgxSmartModalModule,
        BreadcrumbModule,
        NgxLoadingModule,
        SwiperModule
    ],
    declarations: [
        HomeComponent,
        ContatoComponent,
        SobreComponent,
        EmpreendimentosComponent,
        SplendorComponent,
        EleganceComponent,
        PortfolioComponent,
        BlogComponent,
        Page404Component,
        PostComponent,
        FormularioEnvioComponent,
        ContatoFormularioComponent,
        LocalizacaoMapsComponent,
        CardConteudoComponent,
        TrabalheConoscoComponent,
        CampoErroComponent,
        BannerSlideComponent,
        ImageSlideComponent
    ]
})
export class PagesModule { }
