import { PageBase } from '../../infra/page-base';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Title } from '@angular/platform-browser';
import { FirebaseStorageService } from '../../services/firebase-services/firebase-storage.service';
import { tap, finalize } from 'rxjs/operators';
import { TrabalheConosocoModel } from 'app/models/TrabaleConosco';
import { FirebaseFunctionsService } from 'app/services/firebase-services/firebase-functions.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-trabalhe-conosco',
    templateUrl: './trabalhe-conosco.component.html',
    styleUrls: ['./trabalhe-conosco.component.scss']
})
export class TrabalheConoscoComponent extends PageBase implements OnInit {

    formulario: FormGroup;
    nomeArquivoanexo = '';
    fileNameUploaded: string;
    urlFileUploaded: string;
    percetualUpload = 0;
    tamanhoMaximoArquivo = false;
    trabalheConoscoModel = new TrabalheConosocoModel();
    enviandoForm = false;

    constructor(private formBuilder: FormBuilder,
        googleAnalyticsService: GoogleAnalyticsService,
        titleService: Title,
        public router: Router,
        private firebaseStorageService: FirebaseStorageService,
        private firebaseFunctionsService: FirebaseFunctionsService) {
        super(googleAnalyticsService, router, titleService);

        this.addTitlePage('Dicas Imobiliárias, Tendências e Ideias | Epax Construtora');
    }

    ngOnInit() {
        this.formulario = this.formBuilder.group({
            nome: ['', Validators.compose(
                [Validators.required,
                Validators.minLength(2)]
            )],
            email: ['', Validators.compose(
                [Validators.required,
                Validators.email]
            )],
            telefone: ['', Validators.compose(
                [Validators.required,
                Validators.pattern(/^(\(+[0-9]{1,2}\).([0-9]{1,5})\-[0-9]{1,4})$/)]
            )],
            endereco: ['', Validators.compose([Validators.required])],
            cep: ['', Validators.compose([Validators.required])],
            cidade: ['', Validators.compose([Validators.required])],
            fileUpload: ['', Validators.compose([Validators.required])]
        });
    }

    enviar() {
        this.enviandoForm = true;
        if (this.formulario.valid) {
            this.trabalheConoscoModel.nome = this.formulario.controls.nome.value;
            this.trabalheConoscoModel.email = this.formulario.controls.email.value;
            this.trabalheConoscoModel.cep = this.formulario.controls.cep.value;
            this.trabalheConoscoModel.cidade = this.formulario.controls.cidade.value;
            this.trabalheConoscoModel.endereco = this.formulario.controls.endereco.value;
            this.trabalheConoscoModel.telefone = this.formulario.controls.telefone.value;
            this.trabalheConoscoModel.urlAnexo = this.urlFileUploaded;

            this.firebaseFunctionsService.sendEmail(this.trabalheConoscoModel).subscribe(data => {
                ////console.log('envio-email-sucesso', data);
                this.enviandoForm = false;
            }, erro => {
                console.error('envio-email-erro', erro);
                this.enviandoForm = false;
            });

            this.goPage('obrigado');
        }
        else {
            this.enviandoForm = false;
        }
    }

    onFileSelected(event) {
        this.percetualUpload = 0;
        this.tamanhoMaximoArquivo = false;
        this.nomeArquivoanexo = '';
        if (event && event.target && event.target.files.length > 0) {
            if ((event.target.files[0] as File).size <= 1000 * 1024) {
                this.percetualUpload = 0.1;
                this.nomeArquivoanexo = event.target.files[0].name;
                this.uploadFile(event.target.files[0] as File);
                return;
            }

            this.tamanhoMaximoArquivo = true;
        }

        if (this.fileNameUploaded) {
            this.removerArquivo();
            this.fileNameUploaded = '';
        }
    }

    removerArquivo() {
        this.firebaseStorageService.deleteFile(this.fileNameUploaded)
            .subscribe(() => {
                console.log('delete-file-sucess');
                this.fileNameUploaded = '';
                this.nomeArquivoanexo = '';
                this.percetualUpload = 0;
            },
                (erro) => console.log('delete-file-erro', erro));
    }

    private uploadFile(file: File) {
        const objUpload = this.firebaseStorageService.uploadFile(file);
        this.fileNameUploaded = objUpload.fileName;

        objUpload.task.percentageChanges().subscribe(value => {
            this.percetualUpload = value;
        });

        const snapshot = objUpload.task.snapshotChanges().pipe(
            tap(console.log),
            finalize(async () => {
                this.urlFileUploaded = await objUpload.ref.getDownloadURL().toPromise();
                ////console.log('pathUrlFile-uploaded', this.urlFileUploaded);
            }),
        );

        snapshot.subscribe(data => {
            ////console.log('sucesso-upload-file', data);
        }, (error) => console.log('erro-upload-file', error));
    }
}
