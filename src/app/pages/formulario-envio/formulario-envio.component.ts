import { Component, OnInit } from '@angular/core';
import { PageBase } from '../../infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-formulario-envio',
    templateUrl: './formulario-envio.component.html',
    styleUrls: ['./formulario-envio.component.scss']
})
export class FormularioEnvioComponent extends PageBase {

    constructor(googleAnalyticsService: GoogleAnalyticsService, titleService: Title) {
        super(googleAnalyticsService, null, titleService);
        this.addTitlePage('Obrigado pelo seu contato| Epax Construtora');
    }

    ngOnInit() {
    }
}
