import { Component } from '@angular/core';
import { ContatoModel } from 'app/models/ContatoModel';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-sobre',
    templateUrl: './sobre.component.html',
    styleUrls: ['./sobre.component.scss']
})


export class SobreComponent extends PageBase {

    contatoModel: ContatoModel;
    alertMessage: string;

    constructor(googleAnalyticsService: GoogleAnalyticsService,
        _router: Router,
        titleService: Title) {
        super(googleAnalyticsService, _router, titleService);
        this.addTitlePage('Conheça a Nossa História | Epax Construtora');

        this.contatoModel = new ContatoModel();
    }

    ngOnInit() {
    }
}
