import { WordpressService } from 'app/services/wordpress/wordpress.service';
import { ConteudoCardBlog } from 'app/models/ConteudoCardBlog';
import { ConteudoPostBlog } from './../../../models/ConteudoPostBlog';
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { PageBase } from '../../../infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { isArray } from 'util';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})
export class PostComponent extends PageBase {

    postsSugestao = new Array<ConteudoCardBlog>();
    postAtual = new ConteudoPostBlog();
    loading = true;

    constructor(private wp: WordpressService, googleAnalyticsService: GoogleAnalyticsService,
        public _router: Router, public _location: Location,
        private route: ActivatedRoute, titleService: Title) {
        super(googleAnalyticsService, _router, titleService);
        this.addTitlePage('Dicas Imobiliárias, Tendências e Ideias | Epax Construtora');

        const idPost: any = this.route.params['value'].id;
        let postsResult: any[];

        this.wp.getAll(100).subscribe(data => {
            const contentWp: any = data;
            if (isArray(contentWp)) {
                this.postAtual = this.wp.getDataContentCardBlogByID(idPost, contentWp);
                
                postsResult = (contentWp as Array<any>).filter(function(value) {
                    if (value.id != idPost) {
                        return value;
                    }

                    return null;
                });

                this.postsSugestao = this.wp.getDataContentCardBlog(postsResult).slice(0, 2);
                this.loading = false;
            }
        })
    }

    ngOnInit() {
    }

    OnDestroy() {
    }
}
