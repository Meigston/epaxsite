import { Component } from '@angular/core';
import { WordpressService } from 'app/services/wordpress/wordpress.service';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { PageBase } from 'app/infra/page-base';
import { isArray } from 'util';
import { ConteudoCardBlog } from '../../models/ConteudoCardBlog';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.scss']
})

export class BlogComponent extends PageBase {

    posts = new Array<ConteudoCardBlog>();
    allPosts = new Array<ConteudoCardBlog>();
    page = 1;
    totalPages = 1;
    size = 6;

    constructor(private wp: WordpressService,
        googleAnalyticsService: GoogleAnalyticsService,
        titleService: Title) {
        super(googleAnalyticsService, null, titleService);
        this.addTitlePage('Dicas Imobiliárias, Tendências e Ideias | Epax Construtora');

        this.wp.getAll(100)
            .subscribe(data => {
                const contentWp: any = data;
                if (isArray(contentWp)) {
                    this.allPosts = wp.getDataContentCardBlog(contentWp);
                    this.totalPages = Math.round(this.allPosts.length / this.size);
                    this.totalPages = this.totalPages <= 0 ? 1 : this.totalPages;
                    this.pageChange(this.page);
                }
            });
    }

    ngOnInit() {
    }

    pageChange(index: number) {
        const endSize = (index * this.size);
        this.posts = this.allPosts.slice(endSize - this.size, endSize);
    }
}
