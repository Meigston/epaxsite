import { Component } from '@angular/core';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { WordpressService } from '../../services/wordpress/wordpress.service';
import { isArray } from 'util';
import { ConteudoCardBlog } from '../../models/ConteudoCardBlog';
import { Title } from '@angular/platform-browser';
import { BannerContent } from '../../models/BannerContent';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})


export class HomeComponent extends PageBase {

    listaObras = new Array<any>();
    listaLancamentos = new Array<any>();
    posts = new Array<ConteudoCardBlog>();
    slides = [
        new BannerContent('Splendor Residence',
            './assets/img/empreendimentos/01_banner_splendor.png',
            './assets/img/empreendimentos/01_logo_splendor.png',
            '/lancamentos/splendor',
            'O mais novo empreendimento com apartamentos <b>alto padrão em Guarapuava.' +
            '<br/>Grandioso, luxuoso e exclusivo</b>, como você!',
            true),

        new BannerContent('Elegance Residence',
            './assets/img/empreendimentos/elegance/elegance_banner.jpg',
            './assets/img/empreendimentos/invisible_logo.png',
            '/lancamentos/elegance',
            'Conforto, requinte e um lugar completo para viver.<br/>More com exclusividade, ' +
            'tranquilidade e uma vista incrível do Parque do Lago!')
    ]


    constructor(private wp: WordpressService, googleAnalyticsService: GoogleAnalyticsService, titleService: Title) {
        super(googleAnalyticsService, null, titleService);
        this.addTitlePage('A única alto padrão de Guarapuava | Epax Construtora');

        this.wp.getAll(100)
            .subscribe(data => {
                const contentWp: any = data;
                if (isArray(contentWp)) {
                    this.posts = this.wp.getDataContentCardBlog(contentWp).slice(0, 3);
                }
            });

        this.listaObras.push({
            src: './assets/img/portfolio/01_imagem_portal_do_lago.png',
            descricao: 'Portal do lago'
        });

        this.listaObras.push({
            src: './assets/img/portfolio/01_imagem_Tedi.png',
            descricao: 'Tedi'
        });


        this.listaLancamentos.push({
            src: './assets/img/home/01_imagem_Slide_Splendor.png',
            descricao: 'Splendor'
        });

        this.listaLancamentos.push({
            src: './assets/img/empreendimentos/elegance/EPAX_GUAIRA_entrada_v01.jpg',
            descricao: 'Elegance'
        });
    }

    ngOnInit() {
    }
}
