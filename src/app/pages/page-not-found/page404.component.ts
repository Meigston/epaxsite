import { Component, OnInit, Renderer } from '@angular/core';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-page404',
    templateUrl: './page404.component.html',
    styleUrls: ['./page404.component.scss']
})


export class Page404Component extends PageBase {

    constructor(googleAnalyticsService: GoogleAnalyticsService, titleService: Title) {
        super(googleAnalyticsService, null, titleService);
        this.addTitlePage('Página não encontrada | Epax Construtora');
    }

    ngOnInit() {
    }
}