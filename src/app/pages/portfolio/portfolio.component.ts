import { Component, ViewChild, TemplateRef, Renderer } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./portfolio.component.scss']
})


export class PortfolioComponent extends PageBase {

    @ViewChild(TemplateRef) tpl: TemplateRef<any>;

    portfolioSelecionado: {
        id?: number,
        nome?: string,
        imgSrc?: string,
        descricao?: string,
        obra?: string,
        tipo?: string,
        areaLazer?: string[],
        endereco?: string
    };

    showModal = false;
    scrollPage = 0;

    constructor(googleAnalyticsService: GoogleAnalyticsService,
        private ngxSmartModalService: NgxSmartModalService,
        titleService: Title) {
        super(googleAnalyticsService, null, titleService);
        this.addTitlePage('Conheça nossos imóveis em Guarapuava | Epax Construtora');
    }

    ngOnInit() {
    }

    portfolioSelected(id) {
        switch (id) {
            case 1:
                this.portfolioSelecionado = {
                    nome: 'Edifício Portal do Lago',
                    obra: 'OBRA CONCLUÍDA',
                    descricao: 'Um edifício moderno, exclusivo, no centro da cidade e com a mais linda vista, '
                        + 'pertinho do lugar mais aprazível de Guarapuava: o Parque do Lago. O Portal do Lago é um '
                        + 'empreendimento moderno e sofisticado em torres de apartamentos, com unidades exclusivas, '
                        + 'do mais alto padrão, com todo o espaço, segurança e qualidade que você e sua família merecem.',
                    imgSrc: './assets/img/portfolio/01_imagem_portal_do_lago.png',
                    tipo: 'Coberturas duplex e apartamentos com 3 suítes',
                    areaLazer: ['Churrasqueira', 'Fitness Center', 'Party Space'],
                    endereco: 'Rua Coronel Saldanha, 3070 - Santa Cruz - Guarapuava - PR - CEP 85010-130'
                };
                break;

            /*case 2:
                this.portfolioSelecionado = {
                    nome: 'Tedi',
                    obra: 'OBRA CONCLUÍDA',
                    descricao: 'Apartamentos com 3 suítes',
                    imgSrc: './assets/img/portfolio/01_imagem_Tedi.png',
                    tipo: 'Apartamentos com 3 suítes',
                    areaLazer: ['Churrasqueira', 'Sala Estar TV', 'Sala Lareira'],
                    endereco: 'Rua Pau Brasil, 100 - Santana - Guarapuava - PR - CEP 85070-530'
                };
                break;*/

            case 3:
                this.portfolioSelecionado = {
                    nome: 'Residencial Vila Buch',
                    obra: 'OBRA CONCLUÍDA',
                    descricao: 'Oferece um ambiente que torna seu sonho real. Sinônimo de modernidade e sofisticação.',
                    imgSrc: './assets/img/portfolio/01_vila_buch.png',
                    tipo: 'Apartamentos tipo 1 e 2',
                    areaLazer: [],
                    endereco: 'Rua do Bosque, 585 - Bonsucesso - Guarapuava - PR - CEP 85035-380'
                };
                break
        }

        this.scrollPage = window.scrollY + 150;

        if (this.scrollPage <= 100) {
            this.scrollPage = 400;
        }

        this.showModal = true;
        this.ngxSmartModalService.open('myModal');
    }

    close() {
        this.ngxSmartModalService.close('myModal');
        this.showModal = false;
    }
}
