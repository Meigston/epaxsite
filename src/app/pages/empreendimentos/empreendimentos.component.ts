import { Component, OnInit, Renderer } from '@angular/core';
import { DadosObra } from 'app/models/DadosObra';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Router } from '@angular/router';
import { BannerContent } from 'app/models/BannerContent';

@Component({
    selector: 'app-empreendimentos',
    templateUrl: './empreendimentos.component.html',
    styleUrls: ['./empreendimentos.component.scss']
})


export class EmpreendimentosComponent extends PageBase {

    listaObras = new Array<DadosObra>();
    listaLancamentos = new Array<BannerContent>();
    itemSlide: any = {
        src: '',
        secondarysource: '',
        description: '',
        router: ''
    };

    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router) {
        super(googleAnalyticsService, _router)

        const obra1 = new DadosObra();
        obra1.id = 1;
        obra1.nome = 'SPLENDOR RESIDENCE';
        obra1.endereco = 'Rua Guaíra, 4151 - Guarapuava - PR';
        obra1.descricao = 'Personalização, conforto, design inovador e vista definitiva para o Parque do Lago.'
            + '\nViva a melhor experiência de morar em Guarapuava!';
        obra1.metragem = '290,9m² a 631m²';
        obra1.tipo = 'Apartamentos tipo e duplex';
        obra1.unidades_andar = '1 ou 2 unidades por andar';
        obra1.faixa_destaque = 'OBRAS INICIADAS';
        obra1.imagens = [
            {
                src: './assets/img/home/01_imagem_Slide_Splendor.png',
                router: '/lancamentos/splendor',
                text: 'OBRAS INICIADAS'
            },
            {
                src: './assets/img/empreendimentos/splendor/piscina_duplex.jpg',
                router: '/lancamentos/splendor',
                text: 'OBRAS INICIADAS'
            },
            {
                src: './assets/img/empreendimentos/splendor/varanda_com_mureta.jpg',
                router: '/lancamentos/splendor',
                text: 'OBRAS INICIADAS'
            }
        ];
        obra1.router = '/lancamentos/splendor';

        const obra2 = new DadosObra();
        obra2.id = 2;
        obra2.nome = 'ELEGANCE RESIDENCE';
        obra2.endereco = 'Rua Guaíra, 4053 - Guarapuava - PR';
        obra2.descricao = 'Conforto, requinte e um lugar completo para viver. More com exclusividade, '
            + 'tranquilidade e uma vista incrível do Parque do Lago!';
        obra2.metragem = 'A partir de 340m²';
        obra2.tipo = '3 ou 4 suítes';
        obra2.unidades_andar = '3 unidades por andar';
        obra2.faixa_destaque = 'UNIDADES VENDIDAS';
        obra2.imagens = [
            {
                src: './assets/img/empreendimentos/elegance/EPAX_GUAIRA_entrada_v01.jpg',
                router: '/lancamentos/elegance',
                text: 'UNIDADES VENDIDAS'
            },
            {
                src: './assets/img/empreendimentos/elegance/EPAX_GUAIRA_sacada_v02.jpg',
                router: '/lancamentos/elegance',
                text: 'UNIDADES VENDIDAS'
            },
            {
                src: './assets/img/empreendimentos/elegance/EPAX_GUAIRA_estar03B_v01.jpg',
                router: '/lancamentos/elegance',
                text: 'UNIDADES VENDIDAS'
            }
        ];
        obra2.router = '/lancamentos/elegance';

        this.listaObras.push(obra1);
        this.listaObras.push(obra2);

        this.listaLancamentos.push(new BannerContent('Splendor Residence',
            './assets/img/empreendimentos/01_banner_splendor.png',
            './assets/img/empreendimentos/01_logo_splendor.png',
            '/lancamentos/splendor',
            'O mais novo empreendimento com apartamentos <b>alto padrão em Guarapuava.' +
            '<br/>Grandioso, luxuoso e exclusivo</b>, como você!')
        );

        this.listaLancamentos.push(new BannerContent('Elegance Residence',
            './assets/img/empreendimentos/elegance/elegance_banner.jpg',
            './assets/img/empreendimentos/invisible_logo.png',
            '/lancamentos/elegance',
            'Conforto, requinte e um lugar completo para viver.<br/>More com exclusividade, ' +
            'tranquilidade e uma vista incrível do Parque do Lago!')
        );

        this.itemSlide = this.listaLancamentos[0];
    }

    onSlide(event) {
        const index = Number.parseInt(event.current.toString().replace('ngb-slide-', ''));
        this.itemSlide = this.listaLancamentos[index];
    }
}
