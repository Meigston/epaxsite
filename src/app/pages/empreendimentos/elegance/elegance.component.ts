import { Util } from './../../../infra/util';
import { Component, OnInit } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { DadosObra } from 'app/models/DadosObra';
import { Image } from '@ks89/angular-modal-gallery';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { ContatoModel } from 'app/models/ContatoModel';
import { NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap/carousel/carousel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CoordenadasMapas } from 'app/models/CoordenadasMapas';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-elegance',
    templateUrl: './elegance.component.html',
    styleUrls: ['./elegance.component.scss']
})

export class EleganceComponent extends PageBase {

    listaGaleria: Array<object>;
    listaPlantaObra: Image[];
    obraSelecionada: any = {};
    plantasApartamento: any[];
    coordenadas: CoordenadasMapas = {
        latitude: -25.398640,
        longitude: -51.474730,
        zoom: 15
    };
    progressoConstrucao = [];

    contatoModel: ContatoModel;
    alertMessage: string;
    firstLoad = true;

    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router, titleService: Title) {
        super(googleAnalyticsService, _router, titleService);
        this.addTitlePage('Lançamentos de imóveis em Guarapuava | Epax Construtora');

        const imgsGaleria = [
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_entrada_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_brinquedoteca_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_escritorio03_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_estar_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_estar03B_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_jogos_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_piscina_v03.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_playground_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_quadra_v01.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_sacada_v02.jpg',
            './assets/img/empreendimentos/elegance/EPAX_GUAIRA_sacada03B_v01.jpg'
        ];

        this.listaGaleria = [];
        imgsGaleria.forEach((item, index) => {
            this.listaGaleria.push({
                image: item,
                thumbImage: item,
                alt: '',
                title: ''
            });
        });

        this.plantasApartamento = [{
            id: 1,
            tipo: 'Apartamento - Gold',
            metragem: '00 m² (área privativa)',
            suites: '3 suítes',
            bwc: 'Piso aquecido (BWCs e suítes)',
            sacadas: 'Sacadas amplas',
            varanda: 'Varanda gourmet',
            sala: 'Sala de estar',
            vagas_garagem: '3 vagas de garagem',
            imagens: [{
                image: './assets/img/empreendimentos/elegance/plantas/AP_GOLD.jpg',
                thumbImage: './assets/img/empreendimentos/elegance/plantas/AP_GOLD.jpg'
            }]
        },
        {
            id: 2,
            tipo: 'Apartamento - Platinum',
            metragem: '00 m² (área privativa)',
            suites: '4 suítes',
            bwc: 'Piso aquecido (BWCs e suítes)',
            sacadas: 'Sacadas amplas',
            varanda: 'Varanda gourmet',
            sala: 'Ofurô',
            office: 'Office',
            vagas_garagem: '3 vagas de garagem',
            imagens: [{
                image: './assets/img/empreendimentos/elegance/plantas/AP_PLATINUM_ADEGA.jpg',
                thumbImage: './assets/img/empreendimentos/elegance/plantas/AP_PLATINUM_ADEGA.jpg'
            },
            {
                image: './assets/img/empreendimentos/elegance/plantas/AP_PLATINUM_OFFICE.jpg',
                thumbImage: './assets/img/empreendimentos/elegance/plantas/AP_PLATINUM_OFFICE.jpg'
            }]
        }];

        this.progressoConstrucao = [
            {
                descricao: 'Serviços Iniciais',
                valor: 100
            },
            {
                descricao: 'Contenção e Fundação',
                valor: 100
            },
            {
                descricao: 'Estrutura',
                valor: 100
            },
            {
                descricao: 'Alvenaria',
                valor: 100
            },
            {
                descricao: 'Instalações',
                valor: 100
            },
            {
                descricao: 'Revestimentos',
                valor: 100
            },
            {
                descricao: 'Acabamento',
                valor: 100
            },
            {
                descricao: 'Serviços externos',
                valor: 100
            },
            {
                descricao: 'Entrega',
                valor: 100
            }
        ]
        this.listaPlantaObra = [];
        this.obraSelecionada = this.plantasApartamento[0];
        this.contatoModel = new ContatoModel();
    }

    ngOnInit() {
        if (this.firstLoad) {
            this.firstLoad = false;
            window.scroll(0, 0);
        }
    }

    seletedTabBeforeChange(event: NgbTabChangeEvent) {
        this.sendEventGA('unidade-splendor', 'selecionado', event.nextId);
    }

    beforeChange(event: NgbTabChangeEvent) {
    }

    onSlide(slideEvent: NgbSlideEvent) {
        if (slideEvent) {
            const resultItem = this.plantasApartamento.filter(item => (item.id - 1).toString() == slideEvent.current);
            if (resultItem && resultItem.length > 0) {
                this.obraSelecionada = resultItem[0];
            }
        }
    }
}
