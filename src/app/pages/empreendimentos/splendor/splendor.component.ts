import { Component, ViewChild } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { DadosObra } from 'app/models/DadosObra';
import { Image } from '@ks89/angular-modal-gallery';
import { PageBase } from 'app/infra/page-base';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { ContatoModel } from 'app/models/ContatoModel';
import { NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap/carousel/carousel';
import { Router } from '@angular/router';
import { CoordenadasMapas } from '../../../models/CoordenadasMapas';
import { Title } from '@angular/platform-browser';
import { SwiperComponent } from 'ngx-swiper-wrapper';

@Component({
    selector: 'app-splendor',
    templateUrl: './splendor.component.html',
    styleUrls: ['./splendor.component.scss']
})

export class SplendorComponent extends PageBase {

    @ViewChild(SwiperComponent) swiper: SwiperComponent;
    listaGaleria: Array<object>;
    listaPlantaObra: Image[];
    obraSelecionada = new DadosObra();
    plantasApartamento: DadosObra[];

    coordenadas: CoordenadasMapas = {
        latitude: -25.398640,
        longitude: -51.474730,
        zoom: 15
    };

    progressoConstrucao = [];

    contatoModel: ContatoModel;
    alertMessage: string;
    firstLoad = true;

    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router, titleService: Title) {
        super(googleAnalyticsService, _router, titleService);
        this.addTitlePage('Lançamentos de imóveis em Guarapuava | Epax Construtora');

        const imgsGaleria = [
            './assets/img/empreendimentos/splendor/fachada.jpg',
            './assets/img/empreendimentos/splendor/fundo.jpg',
            './assets/img/empreendimentos/splendor/hall.jpg',
            './assets/img/empreendimentos/splendor/sala_jantar.jpg',
            './assets/img/empreendimentos/splendor/sala_widescreen_com_mureta.jpg',
            './assets/img/empreendimentos/splendor/salao_de_festas.jpg',
            './assets/img/empreendimentos/splendor/gourmet_externo.jpg',
            './assets/img/empreendimentos/splendor/lounge_com_mureta.jpg',
            './assets/img/empreendimentos/splendor/varanda_com_mureta.jpg',
            './assets/img/empreendimentos/splendor/sala_intima.jpg',
            './assets/img/empreendimentos/splendor/suite_master.jpg',
            './assets/img/empreendimentos/splendor/sala_de_jogos.jpg',
            './assets/img/empreendimentos/splendor/academia.jpg',
            './assets/img/empreendimentos/splendor/piscina_duplex.jpg',
            './assets/img/empreendimentos/splendor/piscina.jpg'
        ];

        this.listaGaleria = [];
        imgsGaleria.forEach((item) => {
            this.listaGaleria.push({
                image: item,
                thumbImage: item,
                alt: '',
                title: ''
            });
        });

        this.plantasApartamento = [{
            id: 1,
            tipo: 'Tipo 1',
            metragem: '290,96 a 307,11 m² (área privativa)',
            vagas_garagem: '3 vagas de garagem',
            total_unidades: '18 unidades',
            unidades_andar: '2 por andar',
            pavimento: 'Pavimentos 6 a 14',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_1.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_1.png'
            }]
        },
        {
            id: 2,
            tipo: 'Tipo 2',
            metragem: '616,55 m² (área privativa)',
            vagas_garagem: '6 vagas de garagem',
            total_unidades: null,
            unidades_andar: 'Andar exclusivo',
            pavimento: '15º pavimento',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_2.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_2.png'
            }]
        },
        {
            id: 3,
            tipo: 'Tipo 3',
            metragem: '631,09 m² (área privativa)',
            vagas_garagem: '6 vagas de garagem',
            total_unidades: '18 unidades',
            unidades_andar: 'Andar exclusivo',
            pavimento: '16º pavimento',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_3.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_3.png'
            }]
        },
        {
            id: 4,
            tipo: 'Tipo 4',
            metragem: '294,45 a 301,36 m² (área privativa)',
            vagas_garagem: '3 vagas de garagem',
            total_unidades: '3 unidades',
            unidades_andar: '2 por andar',
            pavimento: 'Pavimentos 17 e 19',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_4.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_4.png'
            }]
        },
        {
            id: 5,
            tipo: 'Tipo 5',
            metragem: '297,24 a 307,67 m² (área privativa)',
            vagas_garagem: '3 vagas de garagem',
            total_unidades: '5 unidades',
            unidades_andar: '2 por andar',
            pavimento: 'Pavimentos 25 e 26',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_5.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_tipo_5.png'
            }]
        },
        {
            id: 6,
            tipo: 'Duplex',
            metragem: '566,90 m² (área privativa)',
            vagas_garagem: '6 vagas de garagem',
            total_unidades: '2 unidades',
            unidades_andar: '2 por andar',
            pavimento: 'Pavimentos 25 e 26',
            imagens: [{
                image: './assets/img/empreendimentos/splendor/plantas/planta_baixa_duplex.png',
                thumbImage: './assets/img/empreendimentos/splendor/plantas/planta_baixa_duplex.png'
            }]
        }];

        this.listaPlantaObra = [];
        this.obraSelecionada = this.plantasApartamento[0];

        this.contatoModel = new ContatoModel();

        this.progressoConstrucao = [
            {
                descricao: 'Serviços Iniciais',
                valor: 58.48
            },
            {
                descricao: 'Contenção e Fundação',
                valor: 100
            },
            {
                descricao: 'Estrutura',
                valor: 86.73
            },
            {
                descricao: 'Alvenaria',
                valor: 35.85
            },
            {
                descricao: 'Instalações',
                valor: 0
            },
            {
                descricao: 'Revestimentos',
                valor: 0
            },
            {
                descricao: 'Acabamento',
                valor: 0
            },
            {
                descricao: 'Serviços externos',
                valor: 0
            },
            {
                descricao: 'Entrega',
                valor: 0
            }
        ]
    }

    ngOnInit() {
        if (this.firstLoad) {
            this.firstLoad = false;
            window.scroll(0, 0);
        }
    }

    beforeChange() {
    }

    onSlide(slideEvent: NgbSlideEvent) {
        if (slideEvent) {
            const resultItem = this.plantasApartamento.filter(item => (item.id - 1).toString() == slideEvent.current);
            if (resultItem && resultItem.length > 0) {
                this.obraSelecionada = resultItem[0];
            }
        }
    }
}
