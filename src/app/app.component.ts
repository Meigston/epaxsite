import { Component, OnInit, Inject, Renderer, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import { Location, DOCUMENT } from '@angular/common';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
import { Util } from './infra/util';

declare let ga: Function;


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    private _router: Subscription;
    breadcrumbs: any[];

    @ViewChild(NavbarComponent) navbar: NavbarComponent;

    navbarElement: HTMLElement;
    navbarBreadCumb: HTMLElement;

    constructor(private renderer: Renderer, private router: Router,
        @Inject(DOCUMENT) private document: Document, private element: ElementRef,
        public location: Location,
        private titleService: Title,
        private metaTagService: Meta
    ) {

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                const ignorePage = ['/blog/navigate'];

                if (ignorePage.indexOf(event.urlAfterRedirects) > -1) {
                    return;
                }

                ga('set', 'page', event.urlAfterRedirects);
                ga('send', 'pageview');

                // console.log('GA', 'sended: ' + event.urlAfterRedirects);
                Util.loadBulldeskBody();

                const notPageScrollTop = event.urlAfterRedirects.match(/(#.+[A-Za-z])/g);
                if (notPageScrollTop != null && notPageScrollTop.length > 0) {
                    return;
                }

                setTimeout(() => {
                    window.scroll(0, 0);
                }, 200);
            }
        });
    }

    ngOnInit() {
        this.titleService.setTitle('A única alto padrão de Guarapuava | Epax Construtora');

        // tslint:disable: max-line-length
        /*this.metaTagService.addTags([
            { name: 'Epax', url: 'https://www.epax.com.br', content: 'Construídos nas melhores localizações de Guarapuava, os empreendimentos EPAX são pensados para proporcionar a melhor experiência de morar, com áreas comuns completas e plantas inteligentes para você viver com mais qualidade de vida e tranquilidade. | Epax Construtora' },
            { name: 'Home', url: 'https://www.epax.com.br/', content: 'Procurando um imóvel para comprar? A Epax é a única com apartamentos de alto padrão nos melhores bairros para morar em Guarapuava. Acesse e confira!' },
            { name: 'Sobre', url: 'https://www.epax.com.br/sobre', content: 'A Epax é a única construtora em Guarapuava que possui imóveis verticais de alto padrão. Saiba mais sobre a nossa história.' },
            { name: 'Lançamentos', url: 'https://www.epax.com.br/lancamentos', content: 'Procurando lançamentos imobiliários na cidade de Guarapuava? A Epax Construtora e Incorporadora tem o empreendimento perfeito para você. Acesse e confira!' },
            { name: 'Portifólio', url: 'https://www.epax.com.br/portifolio', content: 'Conheça nossos imóveis à venda em Guarapuava e descubra como é morar bem em um empreendimento da Epax. Entre em contato conosco.' },
            { name: 'Contato', url: 'https://www.epax.com.br/contato', content: 'Procurando imóveis na cidade de Guarapuava? A Epax Construtora e Incorporadora entende a importância de morar bem. Entre em contato!' },
            { name: 'Blog', url: 'https://www.epax.com.br/blog', content: 'As melhores dicas imobiliárias e tendências para quem quer comprar um imóvel alto padrão em Guarapuava. Acesse!' },
            { name: 'Área do Cliente', url: 'https://www.epax.com.br/areadocliente', content: 'Seja bem-vindo! Aqui você pode fazer consultas, realizar solicitações e resolver pendências. Use o seu login e senha para acessar a Área do Cliente. ' },
            { name: 'Erro', url: 'https://www.epax.com.br/erro', content: 'A página que você está procurando pode ter sido removida, teve seu nome alterado ou está temporariamente indisponível.' },
            { name: 'Agradecimento Formulário', url: 'https://www.epax.com.br/obrigado', content: 'Agradecemos por escolher a Epax, a sua construtora e incorporadora em Guarapuava! Em breve entraremos em contato.' },
            { name: 'Trabalhe conosco', url: 'https://www.epax.com.br/trabalheconosco', content: 'Quer fazer parte da equipe Epax Construtora? Cadastre seu currículo e vamos juntos planejar novos imóveis em Guarapuava.' }
        ]);*/

        var ua = window.navigator.userAgent;
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            var version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        if (version) {
            var body = document.getElementsByTagName('body')[0];
            body.classList.add('ie-background');
        }
    }

    removeFooter() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        if (titlee === 'signup' || titlee === 'nucleoicons') {
            return false;
        }
        return true;
    }

    isHome() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '/') {
            titlee = titlee.slice(1);
        }
        else {
            titlee = '';
        }

        if (titlee === 'home' || titlee === '') {
            return true;
        }

        return false;
    }

    containsPath(ulrContains: string) {
        const titlee = this.location.prepareExternalUrl(this.location.path());
        return titlee.search(ulrContains) > -1;
    }

    navbarChange() {
        if (this.containsPath('blog/post')) {
            return;
        }

        const number = window.scrollY;
        if (number > 60 || window.pageYOffset > 60) {
            // add logic
            this.navbarElement.classList.remove('navbar-transparent');

            if (!this.isHome()) {
                this.navbarBreadCumb.classList.remove('navbar-transparent');
                this.navbarBreadCumb.classList.remove('bkg-color');
                this.navbarBreadCumb.classList.add('navbar-solid-color-breadcumb');
            }
        } else {
            // remove logic
            this.navbarElement.classList.add('navbar-transparent');
            this.navbarBreadCumb.classList.add('navbar-transparent');
            this.navbarBreadCumb.classList.add('bkg-color');
            this.navbarBreadCumb.classList.remove('navbar-solid-color-breadcumb');
        }
    }
}
