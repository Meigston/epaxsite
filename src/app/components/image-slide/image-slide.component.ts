import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Component, Input, ViewChild } from '@angular/core';
import { SwiperComponent } from 'ngx-swiper-wrapper';
import { Util } from 'app/infra/util';
import { ImageContent } from 'app/models/ImageContent';
import { PageBase } from 'app/infra/page-base';
import { Router } from '@angular/router';

@Component({
    selector: 'app-image-slide',
    templateUrl: './image-slide.component.html',
    styleUrls: ['./image-slide.component.scss']
})


export class ImageSlideComponent extends PageBase {

    @Input() images: Array<ImageContent>;
    @ViewChild(SwiperComponent) swiper: SwiperComponent;


    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router) {
        super(googleAnalyticsService, _router);
    }

    public indexChange(index) {
        if (index + 1 >= this.images.length) {

            setTimeout(() => {
                this.swiper.directiveRef.setIndex(0);
            }, Util.timeSwiperAutoPlay());
        }
    }
}
