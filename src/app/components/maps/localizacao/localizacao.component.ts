import { Component, Input } from '@angular/core';
import { PageBase } from '../../../infra/page-base';
import { GoogleAnalyticsService } from '../../../services/google-analytics/google-analytics.service';
import { Router } from '@angular/router';
import { CoordenadasMapas } from 'app/models/CoordenadasMapas';

@Component({
    selector: 'app-localizacao-maps',
    templateUrl: './localizacao.component.html',
    styleUrls: ['./localizacao.component.scss']
})

export class LocalizacaoMapsComponent extends PageBase {

    @Input() enderecoText: string;
    @Input() coordenadas: CoordenadasMapas;

    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router) {
        super(googleAnalyticsService, _router);
    }
}
