import { GoogleAnalyticsService } from './../../services/google-analytics/google-analytics.service';
import { Component, Input, ViewChild } from '@angular/core';
import { BannerContent } from '../../models/BannerContent';
import { SwiperComponent } from 'ngx-swiper-wrapper';
import { PageBase } from '../../infra/page-base';

@Component({
    selector: 'app-banner-slide',
    templateUrl: './banner-slide.component.html',
    styleUrls: ['./banner-slide.component.scss']
})


export class BannerSlideComponent extends PageBase {

    @Input() slides: Array<BannerContent>;
    @Input() showSliceTag: boolean;
    @ViewChild(SwiperComponent) swiper: SwiperComponent;

    constructor(googleAnalyticsService: GoogleAnalyticsService) {
        super(googleAnalyticsService);
    }
}
