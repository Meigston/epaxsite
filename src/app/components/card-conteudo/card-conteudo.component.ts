import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PageBase } from '../../infra/page-base';
import { GoogleAnalyticsService } from '../../services/google-analytics/google-analytics.service';
import { ConteudoCardBlog } from '../../models/ConteudoCardBlog';
import { SwiperComponent } from 'ngx-swiper-wrapper';
import { Util } from 'app/infra/util';

@Component({
    selector: 'app-card-conteudo',
    templateUrl: './card-conteudo.component.html',
    styleUrls: ['./card-conteudo.component.scss']
})

export class CardConteudoComponent extends PageBase {
    @Input() posts: Array<ConteudoCardBlog>;
    @Input() groupInCarouselWhenMobile = false;
    @ViewChild(SwiperComponent) swiper: SwiperComponent;

    constructor(googleAnalyticsService: GoogleAnalyticsService, _router: Router) {
        super(googleAnalyticsService, _router);
    }

    goPost(conteudo: ConteudoCardBlog) {
        this.sendEventGA('blog-post', 'acesso', conteudo.titulo);

        this._router.navigateByUrl('/blog/navigate').then(() => {
            this._router.navigate([decodeURI('/blog/post/' + conteudo.id)]);
        });
    }
}
