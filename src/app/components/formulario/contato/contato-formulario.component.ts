import { Component, OnInit, Input } from '@angular/core';
import { PageBase } from '../../../infra/page-base';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from '../../../services/google-analytics/google-analytics.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-contato-formulario',
    templateUrl: './contato-formulario.component.html',
    styleUrls: ['./contato-formulario.component.scss']
})

export class ContatoFormularioComponent extends PageBase implements OnInit {
    @Input() identificador: string;

    formulario: FormGroup;

    constructor(private formBuilder: FormBuilder,
        googleAnalyticsService: GoogleAnalyticsService, _router: Router) {
        super(googleAnalyticsService, _router);

        if (this.identificador) {
            this.identificador = 'Contato';
        }
    }

    ngOnInit() {
        this.formulario = this.formBuilder.group({
            nome: ['', Validators.compose(
                [Validators.required,
                Validators.minLength(2)]
            )],
            email: ['', Validators.compose(
                [Validators.required,
                Validators.email]
            )],
            telefone: ['', Validators.compose(
                [Validators.required,
                Validators.pattern(/^(\(+[0-9]{1,2}\).([0-9]{1,5})\-[0-9]{1,4})$/)]
            )],
            mensagem: ['']
        });
    }

    enviar() {
        if (this.formulario.valid) {
            this.sendEventGA('formulario', this.identificador.toLocaleLowerCase(), 'envio-formulario');
            this.goPage('obrigado');
        }
    }
}
