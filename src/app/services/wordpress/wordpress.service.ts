import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { isArray } from 'util';
import { ConteudoCardBlog } from '../../models/ConteudoCardBlog';
import { ConteudoPostBlog } from '../../models/ConteudoPostBlog';

@Injectable({
    providedIn: 'root'
})
export class WordpressService {

    public URL = 'https://blog.epax.com.br/';
    public API = `${this.URL}wp-json/wp/v2/posts`;

    constructor(private http: HttpClient) { }

    getLastPosts() {
        return this.http.get<any[]>(this.API, {
            params: {
                per_page: '6'
            }
        });
    }

    getAll(id: number) {
        if (!id) {
            id = 1;
        }

        return this.http.get(`${this.API}?_embed&per_page=${id}`);
    }

    getSinglePost(id: number) {
        if (!id) {
            id = null;
        }

        return this.http.get(`${this.API}?_embed&slug=${id}`);
    }

    getDataContentCardBlog(contentPosts: Array<any>): Array<ConteudoCardBlog> {
        const posts = new Array<ConteudoCardBlog>();

        if (!isArray(contentPosts)) {
            return posts;
        }

        contentPosts.forEach(item => {
            if (item && item.content && item.content.rendered) {
                let img = this.extractImageContent(item.content.rendered);
                img = img.replace('class="', 'class="blog-img-card ');
                const dataPost = new Date(item.date);

                posts.push({
                    id: item.id,
                    titulo: item.title.rendered,
                    dataPost: this.formatDate(dataPost),
                    img: img,
                    conteudo: item.excerpt.rendered
                });
            }
        });

        return posts;
    }

    getDataContentCardBlogByID(idPost: number, contentPosts: Array<any>): ConteudoPostBlog {
        if (!isArray(contentPosts)) {
            return new ConteudoPostBlog();
        }

        const posts = contentPosts.filter(function(value) {
            if (value.id == idPost) {
                return value;
            }

            return null;
        });

        if (posts.length === 0) {
            return new ConteudoPostBlog();
        }

        const post = posts[0];
        const img = this.extractImageContent(post.content.rendered);
        const dataPost = new Date(post.date);
        return {
            id: post.id,
            titulo: post.title.rendered,
            dataPost: this.formatDate(dataPost),
            img: img,
            conteudo: post.excerpt.rendered,
            figureWpContent: post.content.rendered,
            textPost: this.extractContentPost(post.content.rendered)
        };
    }

    private extractImageContent(content: string): string {

        return content.match(/(<img src=\").+\/>/g)[0];
    }

    private extractContentPost(content: string): string {
        return content.replace(/<figure.*>.*<\/figure>/gm, '');
    }

    private formatDate(date: Date): string {
        return this.formatNumberDate(date.getDate()) + '/'
            + this.formatNumberDate((date.getMonth() + 1)) + '/'
            + this.formatNumberDate(date.getFullYear())
    }

    private formatNumberDate(numberDate: number): string {
        return numberDate < 10 ? '0' + numberDate : numberDate.toString();
    }
}
