import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage/storage';
import { AngularFireUploadTask, AngularFireStorageReference } from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})

export class FirebaseStorageService {

    constructor(private storage: AngularFireStorage) {
    }

    uploadFile(file: File): { task: AngularFireUploadTask, ref: AngularFireStorageReference, fileName: string } {
        const nameFile = `${Date.now()}_${file.name}`;
        const path = `${environment.pathUpload}/${nameFile}`;
        const ref = this.storage.ref(path);
        const task = this.storage.upload(path, file);

        return { task: task, ref: ref, fileName: nameFile };
    }

    deleteFile(fileName: string) {
        ////const path = `${environment.pathUpload}/${fileName}`;
        return this.storage.ref(environment.pathUpload).child(fileName).delete();
    }
}
