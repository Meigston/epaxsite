import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrabalheConosocoModel } from 'app/models/TrabaleConosco';
import { environment } from '../../../environments/environment';
import { timeout } from 'rxjs/internal/operators/timeout';

@Injectable({
    providedIn: 'root'
})

export class FirebaseFunctionsService {
    constructor(private http: HttpClient) { }

    sendEmail(dados: TrabalheConosocoModel) {
        const dadosEnvio = {
            email: dados.email,
            anexoUrl: dados.urlAnexo,
            dadosFormulario: `Segue dados do formulário prenchido na seção trabalhe concosco:
			<i>Nome</i>: <b>${dados.nome}</b>
			<i>E-mail</i>: <b>${dados.email}</b>
			<i>Telefone</i>: <b>${dados.telefone}</b>
			<i>Cidade</i>: <b>${dados.cidade}</b>
			<i>Cep</i>: <b>${dados.cep}</b>
			<i>Endereço</i>: <b>${dados.endereco}</b>
			<i><b>Segue também anexo informado pelo interessado.</b></i>`
        }
        return this.http.post(environment.urlSendEmail, dadosEnvio)
            .pipe(timeout(30 * 1000));
    }
}
