import { OnInit, HostListener } from '@angular/core';
import { GoogleAnalyticsService } from 'app/services/google-analytics/google-analytics.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Util } from './util';
import { AbstractControl } from '@angular/forms';
import { SwiperComponent } from 'ngx-swiper-wrapper';

// tslint:disable: max-line-length
export class PageBase implements OnInit {

    public widithScreen: number = window.innerWidth;
    public idSetTimeOuSwipe: any;

    ngOnInit(): void {

    }

    constructor(private googleAnalyticsService?: GoogleAnalyticsService,
        public _router?: Router,
        public titleService?: Title) {
    }

    public sendEventGA(eventCategory: string, eventAction: string, eventLabel?: string, eventValue?: number) {
        if (this.googleAnalyticsService !== null && this.googleAnalyticsService !== undefined) {
            this.googleAnalyticsService.eventEmitter(eventCategory, eventAction, eventLabel, eventValue);
        }
    }

    public goPage(url: string) {
        if (this._router == null) {
            console.warn('Page not Found in Router or router no has instance!');
            return;
        }

        this._router.navigate([decodeURI(url)]);
    }

    @HostListener('window:resize', ['$event'])
    public onResize() {
        this.widithScreen = window.innerWidth;
    }

    public addTitlePage(title: string) {
        if (title && this.titleService) {
            this.titleService.setTitle(title);
        }
    }

    public telefoneMask(telefone: AbstractControl) {
        console.log('telefone', telefone);
        if (telefone && telefone.value) {
            telefone.setValue(telefone.value.replace(/\D/gi, ''));

            if (telefone.value.length === 10) {
                telefone.setValue(`(${telefone.value.substring(0, 2)}) ${telefone.value.substring(2, 6)}-${telefone.value.substring(6, telefone.value.length)}`);
            }
            else {
                telefone.setValue(`(${telefone.value.substring(0, 2)}) ${telefone.value.substring(2, 7)}-${telefone.value.substring(7, telefone.value.length)}`);
            }
        }

        return telefone.value ? telefone.value : '';
    }

    public invalidInputForm(controle: AbstractControl): boolean {
        return !controle.valid && controle.touched;
    }

    public indexChange(index, swiper: SwiperComponent, items: []) {

        if (this.idSetTimeOuSwipe) {
            clearTimeout(this.idSetTimeOuSwipe);
            this.idSetTimeOuSwipe = null;
        }

        if (index + 1 >= items.length) {

            this.idSetTimeOuSwipe = setTimeout(() => {
                swiper.directiveRef.setIndex(0);
            }, Util.timeSwiperAutoPlay());
        }
    }
}
