import { AbstractControl } from "@angular/forms";

export class Util {

    static ValidarEmail(email: string) {
        if (email && /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/g.test(email)) {
            return true;
        }

        return false;
    }

    static ValidarTelefone(telefone: string) {
        if (telefone) {
            telefone = telefone.replace(/\D/gi, '');
            if (telefone && /\d/gi.test(telefone) && (telefone.length >= 10)) {
                return true;
            }
        }

        return false;
    }

    static loadBulldeskBody() {
        if (window.document.getElementById('bulldesk-form')) {
            window.document.body.removeChild(window.document.getElementById('bulldesk-form'));

            let s = window.document.createElement('script');
            s.id = 'bulldesk-form';
            s.type = 'text/javascript';
            s.src = '//static.bulldesk.com.br/forms.js';
            window.document.body.appendChild(s);

            console.log('inserido', 'bulldesk-form');
        }

        if (window.document.getElementById('bulldesk-instance')) {
            window.document.body.removeChild(window.document.getElementById('bulldesk-instance'));

            let s = window.document.createElement('script');
            s.id = 'bulldesk-instance';
            s.type = 'text/javascript';
            s.append('new BulldeskFormIntegration({token: \'fbaeaae0ad50322e11f6e696f869eac1\',validate: true,selector: \'#btn-contato-submit\',success: function (response, form, inputs) {console.log(\'bulldesk - started\');},fail: function (response) {console.error(\'bulldesk - fail\', response);}});')
            window.document.body.appendChild(s);

            console.log('inserido', 'bulldesk-instance');
        }
    }

    static timeSwiperAutoPlay(): number {
        return 10 * 1000;
    }
}
