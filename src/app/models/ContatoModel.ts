export class ContatoModel {
    nome: string;
    email: string;
    telefone: string;
    mensagem: string;

    constructor() {
        this.nome = '';
        this.email = '';
        this.telefone = '';
        this.mensagem = '';
    }
}