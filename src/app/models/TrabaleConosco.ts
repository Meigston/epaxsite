export class TrabalheConosocoModel {
    nome: string;
    email: string;
    telefone: string;
    cep: string;
    cidade: string;
    endereco: string;
    urlAnexo: string;
}
