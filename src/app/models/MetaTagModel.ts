import { MetaDefinition } from '@angular/platform-browser';

export class MetaTag implements MetaDefinition {

    [prop: string]: string; charset?: string;
    content?: string;
    httpEquiv?: string;
    id?: string;
    itemprop?: string;
    name?: string;
    property?: string;
    scheme?: string;
    url?: string;
    selector: string;

    constructor(name: string, url: string, content: string) {
        this.content = content;
        this.name = name;
        this.url = url;
    }
}
