export class BannerContent {
    nome: string;
    src: string;
    secondarysource: string;
    router: string;
    description: string;
    bgAttachmentActive: boolean;

    constructor(nome: string,
        src: string, secondarysource: string, router: string, description: string, bgAttachmentActive: boolean = false) {

        this.nome = nome;
        this.src = src;
        this.secondarysource = secondarysource;
        this.router = router;
        this.description = description;
        this.bgAttachmentActive = bgAttachmentActive;
    }

    public srcUrl(): string {
        return 'background-image: url("' + this.src + '") no-repeat;';
    }
}
