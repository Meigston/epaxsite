export class ConteudoPostBlog {
    id: number;
    dataPost: string;
    titulo: string;
    img: string;
    conteudo?: string;
    figureWpContent?: any;
    textPost?: string;
}
