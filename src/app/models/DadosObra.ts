import { Imagem } from './Imagem';

export class DadosObra {
    id: number;
    nome?: string;
    endereco?: string;
    descricao?: string;
    metragem?: string;
    tipo?: string;
    vagas_garagem?: string;
    pavimento?: string;
    total_unidades?: string;
    unidades_andar?: string;
    faixa_destaque?: string;
    imagens?: Array<any>;
    router?: string;

    constructor() {
        this.id = 0;
        this.descricao = '';
        this.nome = '';
        this.endereco = '';
        this.metragem = '';
        this.tipo = '';
        this.unidades_andar = '';
        this.faixa_destaque = '';
        this.imagens = new Array<Imagem>();
        this.router = '';
        this.pavimento = '';
        this.total_unidades = '';
        this.vagas_garagem = '';
    }
}