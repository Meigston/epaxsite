export class Imagem {
    constructor() {
        this.image = "";
        this.alt = "";
        this.title = "";
        this.thumbImage = "";

    }

    image: string;
    thumbImage?: string;
    alt?: string;
    title?: string
}