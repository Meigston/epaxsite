export class CoordenadasMapas {
    latitude: number;
    longitude: number;
    zoom: number;
}