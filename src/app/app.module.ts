import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule, routes } from './app.routing';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';

import { PagesModule } from './pages/page.module';
import { BreadcrumbModule } from 'xng-breadcrumb';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import 'hammerjs';
import 'mousetrap';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { GoogleAnalyticsService } from './services/google-analytics/google-analytics.service';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { NgxLoadingModule } from 'ngx-loading';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Util } from './infra/util';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    loop: true,
    spaceBetween: 30,
    autoplay: {
        delay: Util.timeSwiperAutoPlay(),
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
};

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        RouterModule,
        PagesModule,
        AppRoutingModule,
        BreadcrumbModule,
        BrowserAnimationsModule,
        HttpClientModule,
        GalleryModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDDM7eJgRKmHg1L5tb-WfGwsdoz9-VhqHE' // google maps key
        }),
        RouterModule.forRoot(routes, {
            onSameUrlNavigation: 'reload',
            anchorScrolling: 'enabled'
        }),
        NgImageSliderModule,
        NgxSmartModalModule.forRoot(),
        NgxLoadingModule.forRoot({}),
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SwiperModule
    ],
    providers: [
        GoogleAnalyticsService,
        AngularFireStorage,
        {
            provide: SWIPER_CONFIG,
            useValue: DEFAULT_SWIPER_CONFIG
        }
    ],
    bootstrap: [AppComponent]
})

export class AppModule {

}
