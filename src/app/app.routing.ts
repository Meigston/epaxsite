import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { EmpreendimentosComponent } from './pages/empreendimentos/empreendimentos.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { BlogComponent } from './pages/blog/blog.component';
import { Page404Component } from './pages/page-not-found/page404.component';
import { SplendorComponent } from './pages/empreendimentos/splendor/splendor.component';
import { EleganceComponent } from './pages/empreendimentos/elegance/elegance.component';
import { PostComponent } from './pages/blog/post/post.component';
import { FormularioEnvioComponent } from './pages/formulario-envio/formulario-envio.component';
import { TrabalheConoscoComponent } from './pages/trabalhe-conosco/trabalhe-conosco.component';

export const routes: Routes = [
    { path: '', component: HomeComponent, data: { breadcrumb: 'Home' } },
    { path: 'home', component: HomeComponent, data: { breadcrumb: 'Home' } },

    { path: 'contato', component: ContatoComponent, data: { breadcrumb: 'Contato' } },
    { path: 'contato/portfolio', component: PortfolioComponent, data: { breadcrumb: 'Portfólio' } },

    { path: 'trabalheconosco', component: TrabalheConoscoComponent, data: { breadcrumb: 'Trabalhe conosco' } },

    { path: 'sobre', component: SobreComponent, data: { breadcrumb: 'Sobre' } },
    { path: 'sobre/contato', component: ContatoComponent, data: { breadcrumb: 'Contato' } },
    { path: 'sobre/portfolio', component: PortfolioComponent, data: { breadcrumb: 'Portfólio' } },

    { path: 'lancamentos', component: EmpreendimentosComponent, data: { breadcrumb: 'Lançamentos' } },
    { path: 'lancamentos/splendor', component: SplendorComponent, data: { breadcrumb: 'Splendor' } },
    { path: 'lancamentos/elegance', component: EleganceComponent, data: { breadcrumb: 'Elegance' } },

    { path: 'portfolio', component: PortfolioComponent, data: { breadcrumb: 'Portfólio' } },

    { path: 'blog', component: BlogComponent, data: { breadcrumb: 'Blog' } },
    { path: 'blog/post/:id', component: PostComponent, data: { breadcrumb: 'Post' } },

    { path: 'obrigado', component: FormularioEnvioComponent, data: { breadcrumb: '' } },

    { path: '**', component: Page404Component, data: { breadcrumb: '' } }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes),
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule { }
