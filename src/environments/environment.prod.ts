export const environment = {
    production: true,
    firebaseConfig: {
        apiKey: 'AIzaSyANoYb_JJdNWcexzAlByor5IqTd5UE5VO4',
        authDomain: 'epax-email-attachment.firebaseapp.com',
        databaseURL: 'https://epax-email-attachment.firebaseio.com',
        projectId: 'epax-email-attachment',
        storageBucket: 'epax-email-attachment.appspot.com',
        messagingSenderId: '337343699186',
    },
    pathUpload: 'curriculos-site-epax',
    urlSendEmail: 'https://us-central1-epax-email-attachment.cloudfunctions.net/sendEmailNotification'
};
