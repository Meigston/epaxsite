// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: true,
    firebaseConfig: {
      apiKey: 'AIzaSyANoYb_JJdNWcexzAlByor5IqTd5UE5VO4',
      authDomain: 'epax-email-attachment.firebaseapp.com',
      databaseURL: 'https://epax-email-attachment.firebaseio.com',
      storageBucket: 'epax-email-attachment.appspot.com'
    },
    pathUpload: 'curriculos-site-epax',
    urlSendEmail: 'https://us-central1-epax-email-attachment.cloudfunctions.net/sendEmailNotification'
};
